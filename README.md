# Building

    make
    ./gohw --version
    
# Running

    export HW_LISTEN=:8080
    export HW_MYSQL_CONNSTRING=smth
    export HW_REDIS_ADDRESS=smth
    ./gohw

# Code - helloworld

    import "bitbucket.org/leadteam/gohw/helloworld"

## Usage

#### type Application

```go
type Application struct {
}
```


#### func  NewApplication

```go
func NewApplication(config *Config) (*Application, error)
```
This function is used as a constructor for the application. Here we can
initialize database connection.

#### func (*Application) HelloWorldHandler

```go
func (app *Application) HelloWorldHandler(w http.ResponseWriter, r *http.Request)
```
This function is used to handle incoming requests for the URL defined by the
http.HandleFunc method

#### type Config

```go
type Config struct {
	ListenAt              string
	MySQLConnectionString string
	RedisAddress          string
}
```

This structure is used to store application-scecific configuration obtained
"from the outer space"

#### func  NewConfig

```go
func NewConfig() (*Config, error)
```
A function used as a constuctor (or in our case as a loader) of the config.