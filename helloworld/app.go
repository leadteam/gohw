package helloworld

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"syscall"
)

// This structure is used to store application-scecific
// configuration obtained "from the outer space"
type Config struct {
	ListenAt              string
	MySQLConnectionString string
	RedisAddress          string
	// ...
}

// A function used as a constuctor (or in our case as a loader)
// of the config.
func NewConfig() (*Config, error) {
	var found bool
	var undefinedVars []string

	config := &Config{}

	// get environmental variables values
	config.ListenAt, found = syscall.Getenv("HW_LISTEN")
	if !found {
		undefinedVars = append(undefinedVars, "HW_LISTEN")
	}

	config.MySQLConnectionString, found = syscall.Getenv("HW_MYSQL_CONNSTRING")
	if !found {
		undefinedVars = append(undefinedVars, "HW_MYSQL_CONNSTRING")
	}

	config.RedisAddress, found = syscall.Getenv("HW_REDIS_ADDRESS")
	if !found {
		undefinedVars = append(undefinedVars, "HW_REDIS_ADDRESS")
	}

	// handle undefined variables
	if len(undefinedVars) > 0 {
		for _, v := range undefinedVars {
			log.Println("Environmental variable is not defined:", v)
		}

		return nil, errors.New("Config could not be loaded successfully")
	}

	return config, nil
}

// This structure could contain database objects,
// caches, templates, etc. which should be common
// across handlers.

type Application struct {
	// here we can define for instance
	//
	//    DB *sql.DB
	//
	// which could be accessed via
	//
	//    app.DB
	//
	// where `app` is an Application instance
}

// This function is used as a constructor for the application.
// Here we can initialize database connection.
func NewApplication(config *Config) (*Application, error) {
	// db, err := sql.Open("mysql", config.MySQLConnectionString)
	// if err != nil {
	//     return nil, err
	// }
	app := &Application{
	// db: db
	}

	return app, nil
}

// This function is used to handle incoming requests for the URL
// defined by the http.HandleFunc method
func (app *Application) HelloWorldHandler(w http.ResponseWriter, r *http.Request) {
	// define JSON data
	data := struct {
		Message string `json:"message"`
	}{
		Message: "Hello world",
	}

	// set header
	w.Header().Add("Content-Type", "application/json; charset=utf-8")

	// return json
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		log.Print("HelloWorldHandler:", err)
	}
}
