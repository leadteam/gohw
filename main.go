package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/leadteam/gohw/helloworld"
)

// this constant will be substituted during build
var Version = "N/D"

// main
//
// This function is an entry point for the executable,
// it should reside in the package named `main`.
func main() {
	var err error

	// add --version argument parsing
	var doShowVersion bool

	flag.BoolVar(&doShowVersion, "version", false, "Show application version and exit")
	flag.Parse()

	if doShowVersion {
		fmt.Println(Version)
		return
	}

	// get config
	config, err := helloworld.NewConfig()
	if err != nil {
		// log.Fatal is equivalent to log.Print() followed by a call to os.Exit(1).
		log.Fatal(err)
	}

	// initialize application
	app, err := helloworld.NewApplication(config)
	if err != nil {
		log.Fatal(err)
	}

	// set up handlers
	http.HandleFunc("/hello", app.HelloWorldHandler)

	// run http server
	err = http.ListenAndServe(config.ListenAt, nil)
	if err != nil {
		log.Fatal(err)
	}
}
